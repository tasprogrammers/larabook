@extends('layouts.master')


@section('content')
	
	<div class="col-md-offset-3 col-md-6">
		<div class="panel panel-default">
			<div class="panel-title">Book Details </div>
			<div class="panel-body">
				{{ Form::open() }}

					{{ Form::label("Book Title : ") }}
					{{ Form::text('title', , array('class' => 'form-control')) }}
					{{ Form::label("Book Description : ") }}
					{{ Form::textarea('description', , array('class' => 'form-control')) }}
					{{ Form::label("Book ISBN # : ") }}
					{{ Form::text('isbn', , array('class' => 'form-control')) }}
					{{ Form::label("Publication date : ") }}
					{{ Form::date('publication_date', , array('class' => 'form-control')) }}
					{{ Form::label("Published At : ") }}
					{{ Form::text('published_at', , array('class' => 'form-control')) }}

				{{ Form::close() }}
			</div>
		</div>

	</div><!-- /col-md-6 -->
	

@stop