@extends('layouts.master')


@section('content')
    <div class="col-md-offset-3 col-md-6">
        <div class="panel panel-default">  
            <div class="panel-body">
            
                <h2 align="center">LaraBook :: Please Login</h2>

                {{ Form::open(array('route' => 'auth.login')) }}

                    {{ Form::label('Username :') }}
                    {{ Form::text('username', '',array('class' => 'form-control')) }}

                    {{ Form::label('Password : ') }}

                    {{ Form::password('password',array('class' => 'form-control')) }}

                    <br/>

                    {{ Form::submit('Login',array('class' => 'btn btn-block btn-primary')) }}

                {{ Form::close() }}
                <div style="margin-top:20px;"></div>
                <a href="/signup">Don't have an account ? Signup Now</a>

            </div><!-- /panel-body -->
        </div><!-- /panel -->
    </div><!-- /col-md-6 -->
	

@stop