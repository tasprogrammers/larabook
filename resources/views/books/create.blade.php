@extends('layouts.master')


@section('content')
	

	<h3>New Book Entry </h3>
	<hr/>
	
	<div class="col-md-offset-3 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">Book Details </div>
			<div class="panel-body">

				{{ Form::open(array('route' => 'books.store')) }}

					{{ Form::label("Book Title : ") }}
					{{ Form::text('title', null,array('class' => 'form-control')) }}
					{{ Form::label("Book Description : ") }}

					{{ Form::textarea('description','',  
							array('class' => 'form-control','rows' => '2')) }}
					{{ Form::label("Book ISBN # : ") }}
					{{ Form::text('isbn' , null,array('class' => 'form-control')) }}
					{{ Form::label("Publication date : ") }}
					{{ Form::text('publication_date',null,  array('class' => 'form-control')) }}
					{{ Form::label("Published At : ") }}
					{{ Form::text('published_at', null, array('class' => 'form-control')) }}
					<Br/>
					{{ Form::submit('Save Entry',array('class' => 'btn btn-primary')) }}

				{{ Form::close() }}

			</div>
		</div>

	</div><!-- /col-md-6 -->
	

@stop