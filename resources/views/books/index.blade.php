@extends('layouts.master')


@section('content')
	
	<h3>Browse our Library</h3>
	<p>Knowledge is the intagible version of gold.</p>
	<hr/>
	<div class="row">
		@foreach($books as $book)
		  <div class="col-sm-6 col-md-3">
		    <div class="thumbnail">
		      <img src="/assets/img/placeholder.png" alt=" {{ $book->title }}">
		      <div class="caption">

		        <h3> {{ $book->title }} </h3>
		        <p> {{ str_limit($book->description,80 ) }} </p>

		       <!--  <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p> -->
		      </div><!-- /caption -->
		    </div><!-- /thumbnail -->
		  </div><!-- /col-md-4 -->


		@endforeach

	</div><!-- /row -->

@stop