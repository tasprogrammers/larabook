@extends('layouts.master')


@section('content')
	
	<div class="col-md-10">
		<h3>Join LaraBook</h3><hr/>
		{{ Form::open(array('route' => 'users.signup')) }}
			{{ Form::label('Name') }}
			<div class="row">

				<div class="col-md-4">
					{{ Form::text('fname','',array('class' => 'form-control','placeholder' => 'Firstname')) }}
				</div>

				<div class="col-md-4">
					{{ Form::text('lname','',array('class' => 'form-control','placeholder' => 'Lastname')) }}
				</div>
			</div><!-- /row -->

			<div class="row">
				<div class="col-md-4">
					{{ Form::label('Email') }}
					{{ Form::text('email','',array('class' => 'form-control')) }}
				</div><!-- /col-md-4 -->
				<div class="col-md-4">
					{{ Form::label('Username') }}
					{{ Form::text('username','',array('class' => 'form-control')) }}
				</div><!-- /col-md-4 -->
			</div><!-- /row -->

			<div class="row">
				<div class="col-md-4">
					{{ Form::label('Password') }}
					{{ Form::password('password', array('class '=> 'form-control')) }}
				</div><!-- /col-md-4 -->
			</div><!-- /row -->

			<div class="row">
				<div class="col-md-4">
					{{ Form::label('Re-type Password') }}
					{{ Form::password('password', array('class '=> 'form-control')) }}
				</div><!-- /col-md-4 -->
			</div><!-- /row -->
			<Br/>
			{{ Form::submit('Signup',array('class' => 'btn btn-primary')) }}
				
			</div><!-- /row -->
			
			

		{{ Form::close() }}

	</div><!-- /col-md-8 -->

@stop