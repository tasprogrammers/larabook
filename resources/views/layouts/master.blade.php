<!DOCTYPE html>
<html>
<head>
    <title>
      @if(Session::has('title'))
        {{ Session::get('title') }}
      @else 
        {{ 'LaraBook' }}
      @endif
    </title>

    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
</head>
<body>
    
   <nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">LaraBook</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <!-- <li class="active"><a href="#">Link</a></li> -->
        <li><a href="/">Home </a></li>
        <li><a href="/books"> Books </a></li>
       
      </ul>
      
     @if(Auth::check())
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Howdy, {{ Auth::user()->username }} <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="books/create">Add New Book</a></li>
                <li><a href="auth/logout">Logout</a></li>
               
              </ul>
            </li>
          </ul>
    @endif
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div class="container">

  @if(Session::has('msg'))

    <div class="alert alert-info">

        {{ Session::get('msg') }}

    </div>

  @endif

  @yield('content')


</div><!-- /container -->


<script type="text/javascript" src="/assets/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.js"></script>

<script type="text/javascript">

    $(document).ready(function(){

      $('.alert').delay(1000).slideUp();

    });

</script>


</body>
</html>