@extends('layouts.master')


@section('content')
  <div class="jumbotron">
    <div class="container">
      <h1>LaraBook:: </h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      
    </div><!-- /container -->
  </div><!-- /jumbotron -->

  <h3>Library</h3>


  <table class="table table-hover">
    <thead>
      <tr>
        <th>Book Name</th>
        <th>Date of Publication</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

      @foreach($books as $book)
        <tr>
          <td> {{ $book->title }} </td>
          <td> {{ $book->publication_date }} </td>
          <td>
            {{ link_to_route('books.edit','Edit',array('id' => $book->id),array('class' => 'btn btn-info') )  }}

             {{ 
                Form::open([
                'method' => 'DELETE',
                'route' => ['books.destroy', $book->id]
                ]) 

              }}

            {{ Form::submit('Remove', ['class' => 'btn btn-danger']) }}
            {{ Form::close() }}

            <!-- {{ link_to_route('books.destroy','Remove',array('id' => $book->id)) }} -->
          </td>
        </tr>
      @endforeach

      
    </tbody>
  </table>
  
@stop