<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function() {

    Route::get('/',
    			[
    				'middleware' => 'UserAuth', 
    				'uses'		=> 	'HomeController@index'
    			]

    	);

    Route::get('auth/login',
	    		[
	    			'middleware'	=> 'RedirectAuthUser',
	    			'uses' 			=> 'UsersController@get_login'
	    		]
    	);

    Route::post('auth/login',
    			[
    				'as' => 'auth.login',
    				'uses' => 'UsersController@post_login'
    			]
    	);

    Route::get('signup',['uses' => 'UsersController@get_signup']);
    Route::post('signup',['as' => 'users.signup','uses' => 'UsersController@post_signup']);
    Route::get('auth/logout',['uses' => 'UsersController@logout']);
    Route::resource('books','BooksController');
    
});
