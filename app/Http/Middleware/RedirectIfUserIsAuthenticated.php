<?php

namespace App\Http\Middleware;

use Closure;
use \Illuminate\Support\Facades\Auth as Auth;
use \Illuminate\Support\Facades\Redirect as Redirect;


class RedirectIfUserIsAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {

        if( Auth::check()){

            // user is already logged in
            return Redirect::to('/');

        }

        return $next($request);
    }
}
