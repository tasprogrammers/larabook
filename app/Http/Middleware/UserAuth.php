<?php

namespace App\Http\Middleware;

use Closure;
use \Illuminate\Support\Facades\Auth as Auth;
use \Illuminate\Support\Facades\Redirect as Redirect;


class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard = null)
    {

        if( Auth::guest()){

            return Redirect::to('auth/login');

        }

        return $next($request);
    }
}
