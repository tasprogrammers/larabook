<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Book;

class HomeController extends Controller
{	

	private $book;

	function __construct(Book $book){

		$this->book = $book;
	}

	/**
	 * Show the main page of the application.
	 * @return Response 
	 */
   	public function index()
   	{
   		$books = $this->book->orderBy('created_at')->limit(5)->get();
   		// return $books;
   		return view('index')->with('books',$books);
   	}
}
