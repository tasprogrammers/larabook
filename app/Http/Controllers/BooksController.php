<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Book;
use \Illuminate\Support\Facades\Input as Input;

class BooksController extends Controller
{

    private $book;

    function __construct(Book $book){

        $this->book = $book;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //display the list of books
        $books = $this->book->all();
        return view('books.index')
                ->with('title','LaraBook:: Library')
                ->with('books',$books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create')
                ->with('title','LaraBook::New Book Entry');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->book->create(Input::all());
        return redirect('/')
                ->with('msg','Book has been successfully created !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = $this->book->findOrFail($id);
        return $book;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //find the book 
        $book = $this->book->findOrFail($id);
        return view('books.edit')
                ->with('book',$book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // update the resource 
        $book = $this->book->findOrFail($id);
        $book->fill($request->all());
        $book->save();
        return redirect('/')
                ->with('title','LaraBook::Book Marketplace')
                ->with('msg','Book has been successfully updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // find the entry
        $book = $this->book->findOrFail($id);
        $book->delete();
         return redirect('/')
                ->with('title','LaraBook::Book Marketplace')
                ->with('msg','Book has been successfully deleted !');
    }
}
