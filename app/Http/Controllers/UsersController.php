<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \Illuminate\Support\Facades\Input as Input;
use \Illuminate\Support\Facades\Redirect as Redirect;
use Session;
use Auth;
use App\User;

class UsersController extends Controller
{

  private $user;
  
  function __construct(User $user){

    $this->user = $user;
  }	

	/**
	 * Show the form for user authentication.
	 * @return Response 
	 */
   public function get_login()
   {
   		// Session::flush();
   		return view('auth/login')
   			->with('title','User Login');
   }

   /**
    * Authenticate user.
    * @return Response 
    */
   public function post_login()
   {

	   	// $validator = Validator::make(Input::all(),User::$rules);
		$attempt = Auth::attempt([

					'username' => 	Input::get('username'),
					'password' =>	Input::get('password')
		]);



		if($attempt){

			// user credentials are correct
			return Redirect::to('/')
					->with('msg','You have been successfully logged in.');
		}

		return Redirect::back()
				->with('msg','Invalid username or password.');
				// ->withInputs();
   		
   }

    /**
     * Show the form for user registration.
     * @return Response 
     */     
   public function get_signup()
   {
     return view('users.signup')
            ->with('title','LaraBook:: Join');
   }

   /**
    * Store the newly joined user.
    * @return Response 
    */
   public function post_signup()
   {
     //store user
     $this->user->create(Input::all());
     return redirect('/')
            ->with('title','LaraBook');


   }

   /**
    * Destroy user session.
    * @return Response 
    */
   public function logout()
   {
   		Session::flush();
   		return Redirect::to('auth/login');
   }


}
